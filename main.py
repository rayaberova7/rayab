import requests

from typing import Optional

from fastapi import FastAPI





app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/vegan/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    url = 'https://world.openfoodfacts.org/api/v0/product/'+str(item_id)
    x = requests.get(url)

    reponse = x.json()

    # TODO : determine if vegan 
    return reponse
